---
title: Docker Regular Commands
date: "2020-06-01T01:24:24+08:00"
categories:
  - Study Notes
tags:
  - Docker
# series:
author: Silencer
draft: false
---

## Help command

```shell
docker  version
docker  info
docker  [command] --help   #search manuinfo of chosen command
```
<font color=#ea4335> Offical reference: https://docs.docker.com/engine/reference/commandline/</font>

<!--more-->

## Image Command

#### docker images      -- View all local images.

```shell
silencer@silencer-CloudTerminal:~$ docker images -a
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox_test        latest              291bcda43192        3 months ago        1.22MB
ubuntu              18.04               72300a873c2c        3 months ago        64.2MB
python              2.7                 09a181e16b7e        3 months ago        896MB
hello-world         latest              bf756fb1ae65        4 months ago        13.3kB
busybox             latest              6d5fcfe5ff17        5 months ago        1.22MB

# Options:
  -a, --all             Show all images (default hides intermediate images)
  -q, --quiet           Only show numeric IDs

```

#### docker search IMAGENAME      -- Search  images in community through name.

```shell
silencer@silencer-CloudTerminal:/etc/docker$ docker search mysql
NAME           		 DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
mysql            	 MySQL is a widely used, open-source relation…   9568                [OK]
mariadb          	 MariaDB is a community-developed fork of MyS…   3474                [OK]
mysql/mysql-server	 Optimized MySQL Server Docker images. Create…   700                                     [OK]
...
Options:
  -f, --filter filter   Filter output based on conditions provided
  ex:docker search mysql -f=START=5000
  silencer@silencer-CloudTerminal:/etc/docker$ docker search mysql -f=stars=5000
NAME                DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
mysql               MySQL is a widely used, open-source relation…   9568                [OK]
silencer@silencer-CloudTerminal:/etc/docker$

```

#### docker pull 	IMAGENAME [:tag]      --Download docker images.

```SHELL
silencer@silencer-CloudTerminal:/etc/docker$ docker pull mysql
Using default tag: latest				#If you donot add tags,it will use "latest" by default.
latest: Pulling from library/mysql
afb6ec6fdc1c: Pull complete				#Download layer by layer,the core of docker images! combine filesystem
0bdc5971ba40: Pull complete
97ae94a2c729: Pull complete
f777521d340e: Pull complete
1393ff7fc871: Pull complete
a499b89994d9: Pull complete
7ebe8eefbafe: Pull complete
597069368ef1: Pull complete
ce39a5501878: Pull complete
7d545bca14bf: Pull complete
211e5bb2ae7b: Pull complete
5914e537c077: Pull complete
Digest: sha256:a31a277d8d39450220c722c1302a345c84206e7fd4cdb619e7face046e89031d		#signature
Status: Downloaded newer image for mysql:latest
docker.io/library/mysql:latest			#true address

#equal command
  docker pull mysql
= docker pull docker.io/library/mysql:latest

#Download chosen version of image
silencer@silencer-CloudTerminal:/etc/docker$ docker pull mysql:5.7
5.7: Pulling from library/mysql
afb6ec6fdc1c: Already exists		    #same parts donot download repeatedly
0bdc5971ba40: Already exists
97ae94a2c729: Already exists
f777521d340e: Already exists
1393ff7fc871: Already exists
a499b89994d9: Already exists
7ebe8eefbafe: Already exists
4eec965ae405: Pull complete 		    #only download different parts
a531a782d709: Pull complete
270aeddb45e3: Pull complete
b25569b61008: Pull complete
Digest: sha256:d16d9ef7a4ecb29efcd1ba46d5a82bda3c28bd18c0f1e3b86ba54816211e1ac4
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7


silencer@silencer-CloudTerminal:/etc/docker$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mysql               5.7                 a4fdfd462add        10 days ago         448MB
mysql               latest              30f937e841c8        10 days ago         541MB



```

#### docker rmi 		--delete images

```
docker rmi [-f] IMAGEID                   #delete one image
docker rmi [-f] IMAGEID  IMAGEID ...      #delete servral images
docker rmi [-f] $(docker images -aq)      #delete all images
```



## Container Command
To be continue...
## 版本控制

| 修改内容    | 时间        | 修改人   |
| -------- | --------  | ----- |
| Init       | 2020-06-01T01:24:24+08:00 | Silencer |